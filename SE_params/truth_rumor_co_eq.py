BIRTH = 4
MU = 0.2
ALPHA_t = 0.35
ALPHA_r = 0.35
BETA_t = 0.65
BETA_r = 0.65
GAMMA_t = 0.15
GAMMA_r = 0.15
EPSILON_t = 0.1
EPSILON_r = 0.1
NUM_EXPOSED = 50
NUM_SPREADERS = 150
caption = 'Truth-Rumor Coexistence'

