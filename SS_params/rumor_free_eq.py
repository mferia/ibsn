BIRTH = 2
MU = 0.25
ALPHA_t = 0.6
ALPHA_r = 0.1
BETA_t = 0.6
BETA_r = 0.3
GAMMA_t = 0.15
GAMMA_r = 0.4
DELTA_t = 0.38
DELTA_r = 0.1
NUM_EXPOSED = 100
NUM_SPREADERS = 200
caption = 'Rumor-free Equilibrium'
modelname = 'SS'