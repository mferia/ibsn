from __future__ import unicode_literals
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import random
import sys
import operator
from enum import *
import copy
import os
from SE_params.rumor_free_eq import *

class State(Enum): # define the four states of the nodes.
    Ignorant = 0
    Exposed_r = 1
    Exposed_t = 2
    Spreader_r = 3
    Spreader_t = 4
    Removed = 5
    Out = 6
    
def reset(G):
    """ 
    :param G: The graph to reset
    
    Initialise/reset all the nodes in the network to be ignorant. 
    Used to initialise the network at the start of an experiment
    """
    nx.set_node_attributes(G, name='state', values=State.Ignorant)

        
def initialise_rumor_random(G, num_spreaders = NUM_SPREADERS, num_exposed = NUM_EXPOSED):
    """
    :param G: Graph to infect nodes on
    :param num_spreaders: Number of initial spreaders on graph G
    
    Set the state of a random selection of nodes to be infected. 
    num_spreaders specifices how many spreaders to make, the nodes 
    are chosen randomly from all nodes in the network
    """
    spreader_r_nodes = random.sample(G.nodes(), num_spreaders)
    spreader_t_nodes = random.sample(G.nodes()-spreader_r_nodes, num_spreaders)
    exposed_r_nodes = random.sample(G.nodes()-(spreader_r_nodes+spreader_t_nodes), num_exposed)
    exposed_t_nodes = random.sample(G.nodes()-(spreader_r_nodes+spreader_t_nodes+exposed_r_nodes), num_exposed)
    for n in spreader_r_nodes:
        G.nodes[n]['state'] = State.Spreader_r
    for n in spreader_t_nodes:
        G.nodes[n]['state'] = State.Spreader_t
    for n in exposed_t_nodes:
        G.nodes[n]['state'] = State.Exposed_t
    for n in exposed_r_nodes:
        G.nodes[n]['state'] = State.Exposed_r
    return spreader_r_nodes,spreader_t_nodes


# def initialise_rumor_degree(G, num_spreaders = NUM_TO_SPREAD_TO, largest=True):
#     """
#     :param G: Graph to infect nodes on
#     :param num_to_infect: Number of nodes to infect on G
    
#     Set the state of a selection of nodes to be infected. Nodes are
#     chosen by degree, lasgest degree first.
#     numToInfect specifices how many infections to make, the nodes 
#     are chosen randomly from all nodes in the network
#     """
#     degrees = dict(G.degree()) #get degrees of every node
#     #below we sort the nodes in order of their degree, highest degree first.
#     spreader_nodes = []
#     nodes_sorted_by_degree = sorted(degrees.items(), key=operator.itemgetter(1), reverse=largest) 
#     for x in range(num_spreaders): 
#         spreader_nodes.append(nodes_sorted_by_degree[x])
#     for n in spreader_nodes:
#         G.node[n[0]]['state'] = State.Spreader
#     return spreader_nodes

# def initialise_rumor_betweenness(G, num_to_tell =  NUM_TO_SPREAD_TO, largest=True):
#     #below we sort the nodes in order of their betweenness centrality, highest first.
#     spreader_nodes = []
#     bet_cen = dict(nx.betweenness_centrality(G))
#     nodes_sorted_by_betweenness = sorted(bet_cen.items(), key=operator.itemgetter(1), reverse=largest)
#     for x in range(num_to_tell): 
#         spreader_nodes.append(nodes_sorted_by_betweenness[x])
#     for n in spreader_nodes:
#         G.node[n[0]]['state'] = State.Spreader
#     return spreader_nodes



# def initialise_rumor_w_exposed(G, num_spreaders, num_exposed):
#     """
#     :param G: graph to spread rumour on
#     :param num_spreaders: Number of initial spreaders on graph G
#     :param num_exposed: Number of initial exposed on graph G, exposed do not spread rumour
    
#     Initializes graph with both exposed, and spreaders. 
#     """
#     #initialize spreaders
#     spreader_nodes = random.sample(G.nodes(), num_spreader)
#     for s in spreader_nodes:
#         G.nodes[s]['state'] = State.Spreader
    
#     #initialize exposed
#     exposed_nodes = random.sample(G.nodes(), num_exposed)
#     for e in exposed_nodes:
#         G.nodes[e]['state'] = State.Exposed
    
#     return spreader_nodes, exposed_nodes
    

def spread_model_factory(birth = BIRTH,
                        mu = MU,
                        alpha_r = ALPHA_r,
                        beta_r = BETA_r,
                        gamma_r = GAMMA_r,
                        epsilon_r = EPSILON_r,
                        alpha_t = ALPHA_t,
                        beta_t = BETA_t,
                        gamma_t = GAMMA_t,
                        epsilon_t = EPSILON_t):

    def spread_model(n, G):
        list_to_expose_r = []
        list_spreaders_r = []
        list_to_expose_t = []
        list_spreaders_t = []
        addNode = False
        removeMyself = False
        fadeout = False
        if G.nodes[n]['state'] == State.Spreader_r:
            for k in G.neighbors(n):
                if G.nodes[k]['state'] == State.Ignorant:
                    if random.random() <= alpha_r:
                        list_to_expose_t.append(k)
                elif G.nodes[k]['state'] == State.Exposed_t:
                    if random.random() <= epsilon_t:
                        list_to_expose_r.append(k)
            if random.random() <= gamma_t:
                removeMyself = True
        elif G.nodes[n]['state'] == State.Spreader_t:
            for k in G.neighbors(n):
                if G.nodes[k]['state'] == State.Ignorant:
                    if random.random() <= alpha_t:
                        list_to_expose_r.append(k)
                elif G.nodes[k]['state'] == State.Exposed_r:
                    if random.random() <= epsilon_r:    
                        list_to_expose_t.append(k)
            if random.random() <= gamma_r:
                removeMyself = True
        elif G.nodes[n]['state'] == State.Exposed_r:
            if random.random() <= beta_r:
                list_spreaders_r.append(n)
        elif G.nodes[n]['state'] == State.Exposed_t:
            if random.random() <= beta_t:
                list_spreaders_t.append(n)
        if random.random() <= mu:
            fadeout = True


        # if G.nodes[n]['state'] == State.Ignorant:
        #     for k in G.neighbors(n):
        #         if G.nodes[k]['state'] == State.Spreader_r and random.random() <= alpha_r:
        #             list_to_expose_r.append(k)
        #         if G.nodes[k]['state'] == State.Spreader_t and random.random() <= alpha_t:
        #             list_to_expose_t.append(k)










        return list_to_expose_r,list_to_expose_t,list_spreaders_t,list_spreaders_r,removeMyself,fadeout
    return spread_model

    #rewrite model:
    #consider ignorant nodes first, then their neighbors

def apply_spread(G, 
                list_of_newly_exposed_r,
                list_of_newly_exposed_t, 
                list_of_new_spreaders_r, 
                list_of_new_spreaders_t, 
                list_of_newly_removed,
                list_out,
                birth = BIRTH):
    """    
    Applies the state changes to nodes in the network. Note that the transmission model
    actually builds a list of nodes to infect and to remove.
    """
    for n in list_of_newly_exposed_r:
        G.nodes[n]['state'] = State.Exposed_r
    for n in list_of_newly_exposed_t:
        G.nodes[n]['state'] = State.Exposed_t
    for n in list_of_new_spreaders_r:
        G.nodes[n]['state'] = State.Spreader_r
    for n in list_of_new_spreaders_t:
        G.nodes[n]['state'] = State.Spreader_t
    for n in list_of_newly_removed:
        G.nodes[n]['state'] = State.Removed
    for n in list_out:
        G.nodes[n]['state'] = State.Out
    for i in range(BIRTH):
        add_random_node_birth(G,3)

def get_last_node(G):
    return list(G.nodes)[-1]

def add_random_node_birth(G, max_edges):
    node_to_add = get_last_node(G) + 1
    leng = len(G.nodes)
    G.add_node(node_to_add)
    edges = random.randint(1, max_edges)
    for i in range(edges):
        new_edge = int(random.random()*(len(G.nodes)-1))
        while new_edge == node_to_add:
            new_edge = int(random.random()*(len(G.nodes)-1))
        G.add_edge(node_to_add, new_edge)
    G.nodes[node_to_add]['state'] = State.Ignorant

    

def execute_one_step(G, model):
    """
    :param G: the Graph on which to execute the infection model
    :param model: model used to infect nodes on G

    executes the infection model on all nodes in G
    """
    new_nodes_to_expose_r=[]
    new_nodes_to_expose_t=[] #nodes to infect after executing all nodes this time step  
    new_nodes_to_spreaders_r=[]
    new_nodes_to_spreaders_t=[] #nodes to turn into spreaders after this time step
    new_nodes_to_remove=[] #nodes to set to removed after this time step
    nodes_to_fadeout=[]

    # nodes_to_add = 0
    for n in G:
        exposed_n_r, exposed_n_t ,spreaders_n_r, spreaders_n_t,remove,fadeout = model(n, G)
        new_nodes_to_expose_r = new_nodes_to_expose_r + exposed_n_r
        new_nodes_to_expose_t = new_nodes_to_expose_t + exposed_n_t
        new_nodes_to_spreaders_r = new_nodes_to_spreaders_r + spreaders_n_r
        new_nodes_to_spreaders_t = new_nodes_to_spreaders_t + spreaders_n_t
        if remove:
            new_nodes_to_remove.append(n)
        if fadeout:
            nodes_to_fadeout.append(n)
    apply_spread(G, new_nodes_to_expose_r, new_nodes_to_expose_t, new_nodes_to_spreaders_r, new_nodes_to_spreaders_t, new_nodes_to_remove, nodes_to_fadeout)

def get_infection_stats(G):
    """
    :param G: the Graph on which to execute the infection model
    :returns: a tuple containing three lists of ignorant, exposed, spreader, and removed nodes.

    Creates lists of nodes in the graph G that are ignorant, exposed, spreader, and removed.
    """
    ignorant = []
    exposed_r = []
    exposed_t = []
    spreader_r = []
    spreader_t = []
    removed = []
    for n in G:
        if G.nodes[n]['state'] == State.Ignorant:
            ignorant.append(n)
        elif G.nodes[n]['state'] == State.Exposed_r:
            exposed_r.append(n)
        elif G.nodes[n]['state'] == State.Exposed_t:
            exposed_t.append(n)
        elif G.nodes[n]['state'] == State.Spreader_r:
            spreader_r.append(n)
        elif G.nodes[n]['state'] == State.Spreader_t:
            spreader_t.append(n)
        elif G.nodes[n]['state'] == State.Removed:
            removed.append(n)
    return ignorant, exposed_r, exposed_t, spreader_r, spreader_t, removed


def print_infection_stats(G):
    """
    :param G: the Graph on which to execute the infection model

    Prints the number of succeptible, infected and removed nodes in graph G.
    """
    i,er,et,sr,st,r = get_infection_stats(G)
    print "Ignorant: %d; Exposed to Rumor: %d; Exposed to Truth: %d; Spreader of Rumor: %d; Spreaders of Truth: %d; Removed %d"% (len(i),len(er),len(et),len(sr),len(st),len(r))

def run_spread_simulation(G, model,init, run_visualise=False, normalize = True):

    if init==0:
        initially_infected = initialise_rumor_degree(G)
    elif init==1:
        initially_infected_r,initially_infected_t = initialise_rumor_random(G)
    elif init==2:
        initially_infected = initialise_rumor_betweenness(G)

    i_results = []
    e_r_results = []
    e_t_results = []
    s_r_results = []
    s_t_results = []
    r_results = []

    dt = 0
    i,er,et,sr,st,r = get_infection_stats(G)
    # pos = nx.spring_layout(G, k=.75, scale = 2.0)
    norm = None
    print norm
    stop = 0
    while stop < 50:
        execute_one_step(G, model)
        dt += 1 
        i,er,et,sr,st,r = get_infection_stats(G)
        if normalize:
            norm = float(len(i) + len(er) + len(et) + len(sr) + len(st) + len(r))
        else:
            norm = 1
        i_results.append(len(i)/norm)
        e_r_results.append(len(er)/norm)
        e_t_results.append(len(et)/norm) 
        s_r_results.append(len(sr)/norm)
        s_t_results.append(len(st)/norm) 
        r_results.append(len(r)/norm) 
        # sys.stderr.write('\rIgnorant: %d; \nExposed to Rumor: %d; Exposed to Truth: %d; \nSpreader of Rumor: %d; Spreaders of Truth: %d; \nRemoved: %d; \ntime step: %d'% (len(i),len(er),len(et),len(sr),len(st),len(r),dt))
        # sys.stderr.flush()
        if run_visualise: #If run visualise is true, we output the graph to file
            draw_network_to_file(G,pos,dt,initially_infected)
        stop = stop + 1
    print i_results
    print e_r_results
    print e_t_results
    print s_r_results
    print s_t_results
    print r_results
    # save_to_vid()
    return i_results,e_r_results,e_t_results,s_r_results,s_t_results,r_results, dt, initially_infected_r,initially_infected_t #return our results for plotting



def plot_infection(I,Er,Et,Sr,St,R,G):
    """
    :param I: time-ordered list from simulation output indicating how ignorant count changes over time
    :param E: time-ordered list from simulation output indicating how exposed count changes over time
    :param S: time-ordered list from simulation output indicating how spreader count changes over time
    :param R: time-ordered list from simulation output indicating how removed count changes over time
    :param G: Graph/Network of statistic to plot
   
    Creates a plot of the I,E,S,R output of a spread simulation.
    """
    # peak_exposed_r = max(Er)
    # peak_exposed_t = max(Et)
    # peak_spreaders_r = max(Sr)
    # peak_spreaders_t = max(St)
    # peak_time_exposed_r = Er.index(max(Er))
    # peak_time_exposed_t = Et.index(max(Et))
    # peak_time_spreader_r = Sr.index(max(Sr))
    # peak_time_spreader_t = St.index(max(St))
    # total_infected = I[0]-I[-1]

    fig_size= [10,6]
    plt.rcParams.update({'font.size': 14, "figure.figsize": fig_size})
    xvalues = range(len(I))
    plt.plot(xvalues, I, color='skyblue', linestyle=':', label="I")
    plt.plot(xvalues, Er, color='b', linewidth=0.9, marker='o', label="E rumor")
    plt.plot(xvalues, Et, color='b', linestyle='--', label="E truth")
    plt.plot(xvalues, Sr, color='r', linewidth=0.4, marker='*', label="S rumor")
    plt.plot(xvalues, St, color='r', linewidth=0.7,linestyle='-', label="S truth")
    plt.plot(xvalues, R, color='coral', linestyle='-.', label="R")
    
    # plt.axhline(peak_exposed_r, color='g', linestyle='--', label="Peak Incidence Exposed to Rumor")
    # plt.annotate(str(peak_exposed_r),xy=(1,peak_exposed_r+10), color='g')

    # plt.axhline(peak_exposed_t, color='r', linestyle='--', label="Peak Incidence Exposed to Truth")
    # plt.annotate(str(peak_exposed_t),xy=(1,peak_exposed_t+10), color='r')
    
    # plt.axhline(peak_spreaders_r, color='c', linestyle='--', label="Peak Incidence Spreader of Rumor")
    # plt.annotate(str(peak_spreaders_r),xy=(1,peak_spreaders_r+10), color='c')

    # plt.axhline(peak_spreaders_t, color='m', linestyle='--', label="Peak Incidence Spreader of Truth")
    # plt.annotate(str(peak_spreaders_t),xy=(1,peak_spreaders_t+10), color='m')
    
    # plt.axvline(peak_time_exposed_r, color='g', linestyle=':', label="Peak Time Exposed to Rumor")
    # plt.annotate(str(peak_time_exposed_r),xy=(peak_time_exposed_r+1,8), color='g')

    # plt.axvline(peak_time_exposed_t, color='r', linestyle=':', label="Peak Time Exposed to Truth")
    # plt.annotate(str(peak_time_exposed_t),xy=(peak_time_exposed_t+1,8), color='r')
    
    # plt.axvline(peak_time_spreader_r, color='c', linestyle=':', label="Peak Time Spreader of Rumor")
    # plt.annotate(str(peak_time_spreader_r),xy=(peak_time_spreader_r+1,8), color='c')

    # plt.axvline(peak_time_spreader_t, color='m', linestyle=':', label="Peak Time Spreader of Truth")
    # plt.annotate(str(peak_time_spreader_t),xy=(peak_time_spreader_t+1,8), color='m')
    
    # plt.axhline(total_infected, color='k', linestyle='--', label="Total Exposed")
    # plt.annotate(str(total_infected),xy=(1,total_infected+10), color='k')
    plt.legend()
    plt.xlabel('Time (days)')
    plt.ylabel('Density Evolution')
    plt.title('IESR for network size ' + str(G.order()) +'\n' + caption)
    plt.show()
    
    
def draw_network_to_file(G,pos,t, initially_infected):
    """
    :param G: Graph to draw to png file
    :param pos: position defining how to layout graph
    :param t: current timestep of simualtion (used for filename distinction)
    :param initially_infected: list of initially infected nodes
   
    Draws the current state of the graph G, colouring nodes depending on their state.
    The image is saved to a png file in the images subdirectory.
    """
    # create the layout
    states = []
    for n in G.nodes():
        if n in initially_infected:
            states.append(3)
        else:
            states.append(G.nodes[n]['state'].value)
    from matplotlib import colors
    cmap = colors.ListedColormap(['green', 'blue','red', 'yellow']) 
    """
    green is ignorant,
    blue is exposed,
    red is spreader, 
    yellow is removed
    """
    bounds=[0,1,2,3]

    # draw the nodes and the edges (all)
    nx.draw_networkx_nodes(G,pos,cmap=cmap,alpha=0.5,node_size=50, node_color=states, linewidths = 0, label = "states")
    nx.draw_networkx_edges(G,pos,alpha=0.075)
    plt.annotate("alpha: " + str(ALPHA), xy=(-2,2.2), color="k")
    plt.annotate("beta: " + str(BETA), xy=(-1.0,2.2), color="k")
    plt.annotate("gamma: " + str(ALPHA), xy=(0.0,2.2), color="k")
    # plt.annotate("mu: " + str(MU), xy=(0.5,2.2), color="k")
    # plt.annotate("network size: " + str(n), xy=(-2.0,2), color="k")
    plt.savefig("SIM_OUTPUT/imagesg"+str(t)+".png")
    plt.clf()


def plotDistribution(_influences):
    plt.hist(_influences,range=[0,1],bins=30)
    plt.title("Realisations")
    plt.xlabel("Number of infected nodes / Total number of nodes")
    plt.ylabel("Frequency")
    # plt.savefig("dist_plot.png")
    plt.show()

def save_to_vid():
    os.system("ffmpeg -r 10 -i SIM_OUTPUT/imagesg%1d.png -vcodec rawvideo -y SIM_OUTPUT/VIDEO/movie.mp4")
