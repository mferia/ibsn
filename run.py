from models.IESRMODEL_SS import *

N = 1000
# nw = nx.barabasi_albert_graph(N, 2)
nw = nx.complete_graph(N)
# nw = nx.erdos_renyi_graph(N, 0.01)
m = spread_model_factory()
reset(nw) # initialise all nodes to succeptible
norm = True
I,Er,Et,Sr,St,R,endtime= run_spread_simulation(nw, m,1,normalize=norm)
plot_infection(I,Er,Et,Sr,St,R,nw)

